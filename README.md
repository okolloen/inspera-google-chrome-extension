# Inspera - Google Chrome extension

Adds keyboard shortcuts to Inspera when used with Google Chrome

## Keyboard shortcuts

### Next/previous candidate
PageDown/Arrow down - Move to next candidate
PageUp/ArrowUp - Move to previous candidate

### Next/previous assignment
ArrowRight - Move to next task/assignment
ArrowLeft - Move to previous task/assignment

### Download uploaded assignment
Alt+Shift+D - When on a page with an uploaded file, download this file as a zip file, using the student number as file name. This is something I do often as Inspera is to be used for all grade giving assignments. The students upload a single zip file with the solution to the task given.
 
### Grade assignment
Keys A-F - Grade an uploaded assignment (note, not tested for grading multiple question exams.)

## Installing in Chrome
 
Download the repo, then in Chrome select "More tools/Extensions" ("Flere verkt�y/Utvidelser" in the norwegian edition). 
 
Check of for "Developer mode" ("Utviklermodus" in the Norwegian edition), then click "Load unpacked extension" ("Last inn upakket utvidelse" in the norwegian edition). This will load the extension and the keyboard shortcuts should be working.