var allLinks = [];
var found = 0;

function downloadAs() {
  if (allLinks.length==2) {
    let fname = '';
    let url = '';
    if (allLinks[0].indexOf("http:")==0) {
      url = allLinks[0];
      fname = allLinks[1]+'.zip';
    } else {
      url = allLinks[1];
      fname = allLinks[0]+'.zip';
    }
    chrome.downloads.download({url: url, filename: fname, saveAs: true});
  }
}

// Add links to allLinks and visibleLinks, sort and show them.  send_links.js is
// injected into all frames of the active tab, so this listener may be called
// multiple times.
chrome.extension.onRequest.addListener(function(links) {
  for (var index in links) {
    allLinks.push(links[index]);
    found++;
    if (found==2) {
      downloadAs();
    }
  }
  allLinks.sort();
  visibleLinks = allLinks;
});

chrome.commands.onCommand.addListener(function(command) {
  if (command=='toggle-feature') {
    allLinks = [];
    found = 0;
    chrome.windows.getCurrent(function (currentWindow) {
      chrome.tabs.query({active: true, windowId: currentWindow.id},
                        function(activeTabs) {
        if (activeTabs[0].title.indexOf('Inspera')==0) {
          chrome.tabs.executeScript(
            activeTabs[0].id, {file: 'send_links.js', allFrames: true});
        }
      });
    });
  }
  console.log('onCommand event received for message: ', command);
});
