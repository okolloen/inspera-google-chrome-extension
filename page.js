
document.addEventListener('keydown', e=>{
  let navigation = document.querySelectorAll('#footer a.ia-footeritem');
  if (navigation.length==0) {  // We are in an iframe, find it from parent frame
    navigation = window.parent.document.querySelectorAll('#footer a.ia-footeritem');
  }
  if (navigation.length>0) {
    if (e.key == 'PageDown' || e.key == 'ArrowDown') {  // Next candidate
      navigation[2].click();
    }
    if (e.key == 'PageUp' || e.key == 'ArrowUp') {      // Prev candidate
      navigation[0].click();
    }
    if (e.key == 'ArrowLeft'&&navigation.length>3) {                             // Prev task
      navigation[3].click();
    }
    if (e.key == 'ArrowRight'&&navigation.length>3) {                            // Next task
      navigation[4].click();
    }
  }
  let grades = document.querySelectorAll('.grade-grid-wrapper.final-grade span');
  if (grades.length==0) {
    grades = window.parent.document.querySelectorAll('.grade-grid-wrapper.final-grade span')
  }

  if (grades.length==0) {
    grades = document.querySelectorAll('#fluidSideContentWrapper .grade-grid span'); // .graderSidebar .grade-graderSidebar div')
  }
  if (grades.length==0) {
    grades = window.parent.document.querySelectorAll('#fluidSideContentWrapper .grade-grid span')
  }
  if (grades.length>0) {
    let key = e.key.toUpperCase();
    if (key == 'F'||key == 'E'||key == 'D'||key == 'C'||key == 'B'||key == 'A') {
      grades.forEach(grade=>{
        if (grade.innerHTML==key)
          grade.click();
      });
    }
  }
});
